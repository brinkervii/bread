from bs4 import BeautifulSoup


class HtmlFile:
    _path: str
    _content: str

    def __init__(self, path: str, content: str):
        self._path = path
        self._content = content

    @property
    def path(self) -> str:
        return self._path

    @property
    def content(self):
        return self._content

    def soup(self) -> BeautifulSoup:
        return BeautifulSoup(self._content, "html5lib")

    def with_new_content(self, new_content: str) -> "HtmlFile":
        cls = type(self)

        return cls(self._path, new_content)

    @classmethod
    def from_source(cls, path: str, content: str):
        return cls(path, content)

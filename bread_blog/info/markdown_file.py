from copy import deepcopy
from datetime import date
from pathlib import Path
from string import Template
from typing import Any, Dict, Optional, List

import markdown
import yaml

from bread_blog.models.state import State
from bread_blog.util.path_util import change_extension, without_extension


def _remove_carriage_return(s: str) -> str:
    return s.strip("\r")


def _rebuild_lines(lines):
    return "\n".join(list(map(lambda ln: ln[1], lines)))


_html_template = Template("""\
{% extends "$LAYOUT_PATH" %}

{% block article %}
$MD_HTML
{% endblock %}
""")

Frontmatter = Dict[str, Any]


class MarkdownFile:
    _path: str
    _content: str
    _frontmatter: Frontmatter

    def __init__(self, path: str, content: str, frontmatter: Optional[Frontmatter] = None):
        self._path = path
        self._content = content
        self._frontmatter = frontmatter or {}

        self._fix_frontmatter()

    def _fix_frontmatter(self):
        fm_date = self._frontmatter.get("date", None)
        if isinstance(fm_date, str):
            self._frontmatter["date"] = date.fromisoformat(fm_date)

        else:
            self._frontmatter["date"] = date.today()

        fm_title = self._frontmatter.get("title", None)
        if fm_title is None:
            self._frontmatter["title"] = without_extension(Path(self._path)).name

        self._frontmatter["subtitle"] = self._frontmatter.get("subtitle", "")

    def render(self, state: State) -> str:
        rendered_markdown = markdown.markdown(
            self._content,
            extensions=[
                "markdown.extensions.tables",
                "markdown.extensions.fenced_code",
                "markdown.extensions.codehilite",
                "markdown.extensions.smarty",
                "markdown.extensions.toc",
                "mdx_truly_sane_lists",
            ],
        )

        html_content = _html_template.safe_substitute({
            "MD_HTML": rendered_markdown,
            "LAYOUT_PATH": state.options.markdown_layout_path
        })

        return html_content

    @property
    def path(self) -> str:
        return self._path

    @property
    def html_path(self) -> str:
        return str(change_extension(Path(self._path), "html"))

    @property
    def tags(self) -> List[str]:
        return list(filter(None, map(str.strip, self._frontmatter.get("tags", None) or [])))

    @property
    def frontmatter(self):
        return deepcopy(self._frontmatter)

    @classmethod
    def from_source(cls, path: str, source: str):
        lines = list(
            enumerate(
                map(
                    _remove_carriage_return,
                    source.split("\n")
                )
            )
        )

        frontmatter_separators = list(
            filter(
                lambda t: t[1].strip() == "---",
                lines
            )
        )

        have_frontmatter = len(frontmatter_separators) > 0
        if have_frontmatter:
            if frontmatter_separators[0][0] == 0 and len(frontmatter_separators) > 1:
                frontmatter_slice = lines[1:frontmatter_separators[1][0]]
                content_slice = lines[frontmatter_separators[1][0] + 1:]

            else:
                frontmatter_slice = lines[:frontmatter_separators[0][0]]
                content_slice = lines[frontmatter_separators[0][0] + 1:]

        else:
            frontmatter_slice = []
            content_slice = lines

        try:
            frontmatter = yaml.load(_rebuild_lines(frontmatter_slice), Loader=yaml.CSafeLoader)

        except yaml.YAMLError as e:
            print(e)
            frontmatter = {}

            content_slice = lines

        return cls(
            path=path,
            content=_rebuild_lines(content_slice),
            frontmatter=frontmatter
        )

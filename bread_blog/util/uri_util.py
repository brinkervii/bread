from pathlib import Path


def change_extension(uri: str, new_extension: str):
    parts = uri.split("/")

    if len(parts) == 1:
        directory = []
        name = parts[0]

    else:
        directory = parts[:-1]
        name = parts[-1]

    split_name = name.split(".")

    if len(split_name) == 1:
        new_name = split_name[0]

    else:
        name_parts = split_name[:-1]
        name_parts = [*name_parts, new_extension]
        new_name = ".".join(name_parts)

    parts = [*directory, new_name]

    return "/".join(parts)


def to_path(uri: str):
    return Path(*uri.split("/"))

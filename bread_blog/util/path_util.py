from pathlib import Path


def change_extension(path: Path, new_extension: str) -> Path:
    name_split = path.name.split(".")

    if len(name_split) == 1:
        return path

    name_without_extension = name_split[:-1]
    name_parts = [*name_without_extension, new_extension]
    new_name = ".".join(name_parts)

    return path.parent / new_name


def without_extension(path: Path):
    name_parts = path.name.split(".")

    name_parts_without_extension = name_parts[:-1]
    if len(name_parts_without_extension) <= 0:
        return path

    name_without_extension = ".".join(name_parts_without_extension)

    return path.parent / name_without_extension

from typing import List


def split_by_slash(link: str) -> List[str]:
    return list(filter(None, map(str.strip, link.split("/"))))

import logging
import uuid
from logging import Logger
from typing import List

from bread_blog.models.state import State
from bread_blog.etl.task import Task


class TaskRunner:
    _id: str
    _log: Logger
    _tasks: List[Task]

    def __init__(self):
        self._id = str(uuid.uuid4())
        self._log = logging.getLogger(f"{type(self).__name__} (id={self._id})")
        self._tasks = []

    def add_tasks(self, tasks: List[Task]):
        self._tasks.extend(tasks)

    def run(self, state: State):
        for task in self._tasks:
            self._log.info(f"Running task {type(task).__name__}")
            task.call_this_to_run_task(state)

    def collect_results(self, task_id: str):
        return list(
            filter(
                None,
                map(
                    lambda t: t.result,
                    filter(
                        lambda t: t.id == task_id,
                        self._tasks
                    )
                )
            )
        )

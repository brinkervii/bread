import logging
import uuid
from abc import ABC, abstractmethod
from logging import Logger
from typing import List, Optional, Any

from bread_blog.models.state import State

TaskDependencies = List[str]


class Task(ABC):
    _id: str
    _instance_id: str
    _log: Logger
    _depends_on: TaskDependencies
    _result: Optional[Any] = None

    def __init__(self, task_id: str, depends_on: Optional[TaskDependencies] = None):
        self._id = task_id
        self._instance_id = str(uuid.uuid4())
        self._log = logging.getLogger(f"{type(self).__name__} (id={self._instance_id})")
        self._depends_on = list(depends_on or [])

    @abstractmethod
    def run(self, state: State) -> Any:
        ...

    def call_this_to_run_task(self, state: State):
        self._result = self.run(state)

    @property
    def result(self) -> Optional[Any]:
        return self._result

    @property
    def id(self) -> str:
        return self._id

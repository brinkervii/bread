import json
from dataclasses import dataclass, asdict
from pathlib import Path


@dataclass
class BuildOptions:
    project_root: Path
    output_root: Path

    markdown_layout_path: str = "layout/_article.html"

    def json(self, **kwargs):
        kwargs = {
            **kwargs,
            "default": str
        }

        return json.dumps(asdict(self), **kwargs)

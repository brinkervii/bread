from dataclasses import dataclass, field
from typing import Callable, List, Dict, Any

from fs.memoryfs import MemoryFS
from fs.multifs import MultiFS
from fs.osfs import OSFS

from bread_blog.models.build_options import BuildOptions
from bread_blog.models.project_configuration import ProjectConfiguration


@dataclass
class State:
    options: BuildOptions
    configuration: ProjectConfiguration

    build_fs: MemoryFS = field(default_factory=lambda: MemoryFS())
    processed_paths: List[str] = field(default_factory=list)
    markdown_provider: Callable = None
    html_provider: Callable = None
    template_variables: Dict[str, Dict[str, Any]] = field(default_factory=dict)

    _project_fs: OSFS = None

    @property
    def project_fs(self) -> OSFS:
        if self._project_fs is not None:
            return self._project_fs

        self._project_fs = OSFS(root_path=str(self.options.project_root.resolve()))

        return self._project_fs

    @property
    def multi_fs(self) -> MultiFS:
        fs = MultiFS(auto_close=False)
        fs.add_fs("project", self.project_fs)
        fs.add_fs("build", self.build_fs)

        return fs

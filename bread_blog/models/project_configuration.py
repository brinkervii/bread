from typing import Optional, List

from pydantic import BaseModel, Field


class ProjectConfiguration(BaseModel):
    name: str
    author: str
    project_root: Optional[str] = "."
    output_root: Optional[str] = "dist"
    prefix: Optional[str] = None
    contributors: Optional[List[str]] = Field(default_factory=list)

from pathlib import Path
from typing import Any, Optional

from bs4 import BeautifulSoup

from bread_blog.models.state import State
from bread_blog.etl.task import Task
from bread_blog.util.string_util import split_by_slash


def _make_link_absolute(file_path: str, link: str, prefix: str) -> str:
    if link.lower().startswith("http"):
        return link

    if not link.startswith("/"):
        directory = Path(file_path).parent
        absolute_path_to_file = directory / link

        link = str(absolute_path_to_file)

    link_parts = [
        "",
        *split_by_slash(prefix),
        *split_by_slash(link)
    ]

    return "/".join(link_parts)


def _process_soup(file_path: str, soup: BeautifulSoup, prefix: Optional[str] = "/") -> BeautifulSoup:
    for tag in soup.find_all():
        if not hasattr(tag, "attrs"):
            continue

        if src := tag.attrs.get("src", None):
            tag.attrs["src"] = _make_link_absolute(file_path, src, prefix)

        elif href := tag.attrs.get("href", None):
            tag.attrs["href"] = _make_link_absolute(file_path, href, prefix)

    return soup


class MakeLinksAbsolute(Task):
    def run(self, state: State) -> Any:
        for html_file in state.build_fs.walk.files(filter=["*.html"]):
            soup = _process_soup(
                html_file,
                BeautifulSoup(state.build_fs.readtext(html_file), "html5lib")
            )

            new_content = soup.prettify("UTF-8")

            state.build_fs.writebytes(html_file, new_content)

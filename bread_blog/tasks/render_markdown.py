import re
from pathlib import Path
from typing import Any
from typing import List, Union

from bs4 import BeautifulSoup
from fs.errors import DirectoryExists

from bread_blog.info.markdown_file import MarkdownFile
from bread_blog.models.state import State
from bread_blog.etl.task import Task


class Summarizer:
    _words: List[str]
    _character_counter: int = 0
    _limit: int
    _break_pads: List[str]

    def __init__(self, limit: int = 50, break_pads: Union[List[str], None] = None):
        self._words = []
        self._limit = limit

        self._break_pads = list(set(map(str.strip, break_pads))) if break_pads else []

    @property
    def limit_reached(self) -> bool:
        return self._character_counter >= self._limit

    @property
    def content(self) -> str:
        return " ".join(self._words)

    def add(self, words: str):
        for word in re.split(r"\s+", words):
            word = word.strip()
            length = len(word)

            for bp in self._break_pads:
                if word.lower() == bp.lower():
                    return

            if self._character_counter + length < self._limit:
                self._words.append(word)
                self._character_counter += length

            else:
                return

    def __str__(self):
        return self.content


class RenderMarkdown(Task):
    def run(self, state: State) -> Any:
        markdown_files: List[MarkdownFile] = state.markdown_provider()

        number_of_files = len(markdown_files)
        self._log.info(f"Rendering {number_of_files} markdown file{'' if number_of_files == 1 else 's'}")

        for file in markdown_files:
            path = Path(file.html_path)
            self._log.info(f"Rendering markdown file '{file.path}' to '{path}'")

            try:
                state.build_fs.makedirs(str(path.parent))

            except DirectoryExists:
                ...

            fm = file.frontmatter

            new_content = file.render(state)

            soup = BeautifulSoup(new_content, "html5lib")
            summarizer = Summarizer(break_pads=["[summary-snip]"])
            summarizer.add(soup.text)

            state.build_fs.writetext(str(path), new_content)
            state.template_variables[str(path)] = {
                "article": {
                    "href": str(path),
                    "front_matter": fm,
                    "title": fm["title"],
                    "subtitle": fm["subtitle"],
                    "date": fm["date"],
                    "summary": summarizer.content
                }
            }
            state.processed_paths.append(file.path)

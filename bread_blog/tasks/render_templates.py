from pathlib import Path
from typing import Any, Optional, Callable, Tuple
from typing import List

from fs.multifs import MultiFS
from jinja2 import Environment, select_autoescape, BaseLoader

from bread_blog.info.html_fille import HtmlFile
from bread_blog.models.state import State
from bread_blog.etl.task import Task


class MultiFSLoader(BaseLoader):
    _fs: MultiFS

    def __init__(self, fs: MultiFS):
        self._fs = fs

    def get_source(
            self,
            environment: "Environment",
            template: str
    ) -> Tuple[str, Optional[str], Optional[Callable[[], bool]]]:
        content = self._fs.readtext(template)
        filename = Path(template).name

        def uptodate():
            return False

        return content, filename, uptodate

    def list_templates(self) -> List[str]:
        return super().list_templates()


class RenderTemplates(Task):
    def run(self, state: State) -> Any:
        files: List[HtmlFile] = state.html_provider()
        new_files: List[HtmlFile] = []

        environment = Environment(
            loader=MultiFSLoader(state.multi_fs),
            autoescape=select_autoescape(),
            cache_size=0
        )

        for file in files:
            path = Path(file.path)
            if path.name.startswith("_"):
                continue

            self._log.info(f"Rendering template '{file.path}'")

            template = environment.from_string(file.content)
            template_variables = state.template_variables.get(file.path, None) or {}

            rendered_content = template.render(template_variables)
            new_files.append(file.with_new_content(rendered_content))

        for file in new_files:
            state.build_fs.writetext(file.path, file.content)

        return new_files

from typing import Any

from bread_blog.info.html_fille import HtmlFile
from bread_blog.models.state import State
from bread_blog.etl.task import Task


class CollectHtml(Task):
    def run(self, state: State) -> Any:
        fs = state.multi_fs

        files = []

        for path in fs.walk.files(filter=["*.html"]):
            self._log.info(f"Collecting html file '{path}'")

            files.append(
                HtmlFile.from_source(
                    path=path,
                    content=fs.readtext(path)
                )
            )

        return files

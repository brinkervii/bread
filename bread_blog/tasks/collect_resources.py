import logging
import re
from pathlib import Path

from bs4 import BeautifulSoup, Tag
from fs import copy
from fs.errors import DirectoryExists

from bread_blog.models.state import State
from bread_blog.etl.task import Task

log = logging.getLogger(__name__)


def _do_copy_resource(state: State, path: str, src: str):
    if re.search(r"\w+://.*", src):
        return

    elif not src.startswith("/"):
        resource_path = src.lstrip("/")

    else:
        resource_path = str(Path(path).parent / src)

    try:
        resource_parent = str(Path(resource_path).parent)
        state.build_fs.makedirs(resource_parent)

    except DirectoryExists:
        ...

    if state.multi_fs.exists(resource_path) and state.multi_fs.isfile(resource_path):
        copy.copy_file(
            state.project_fs, resource_path,
            state.build_fs, resource_path
        )


class CollectResources(Task):
    def run(self, state: State):
        for path in state.build_fs.walk.files(filter=["*.html"]):
            soup = BeautifulSoup(state.build_fs.readtext(path), "html5lib")

            tag: Tag
            for tag in soup.find_all():
                if not hasattr(tag, "attrs"):
                    continue

                if src := tag.attrs.get("src", None):
                    self._log.info(f"Attempting to include resource {src}")
                    _do_copy_resource(state, path, src)

                elif (href := tag.attrs.get("href", None)) and (rel := tag.attrs.get("rel", None)):
                    rel = rel[0]
                    if rel.strip().lower() == "icon":
                        self._log.info(f"Attempting to include resource {href}")
                        _do_copy_resource(state, path, href)

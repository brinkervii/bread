import logging
from dataclasses import dataclass
from pathlib import Path
from typing import Any

import sass
from fs.base import FS
from fs.errors import DirectoryExists

from bread_blog.info.html_fille import HtmlFile
from bread_blog.models.state import State
from bread_blog.etl.task import Task
from bread_blog.util import uri_util, path_util

log = logging.getLogger(__name__)


@dataclass
class CompileSCSSResult:
    input_path: Path = None
    output_path: Path = None
    output_href: str = None

    error: bool = False
    skip: bool = False
    message: str = ""


def _compile_scss(input_fs: FS, output_fs: FS, href: str) -> CompileSCSSResult:
    res = CompileSCSSResult()

    if href.lower().startswith("http"):
        res.skip = True
        res.message = "Href is remote content"

        return res

    if not href.lower().endswith(".scss"):
        res.skip = True
        res.message = "File is not a sass stylesheet"

        return res

    res.input_path = uri_util.to_path(href)
    res.output_path = path_util.change_extension(res.input_path, "css")
    res.output_href = uri_util.change_extension(href, "css")

    def importer(*args, **kwargs):
        raise NotImplementedError()  # TODO: Implement SASS importer

    output_style = sass.compile(
        string=input_fs.readtext(href),
        output_style="compact",
        source_map_embed=True,
        importers=((0, importer),)
    )

    try:
        output_fs.makedirs(str(res.output_path.parent))

    except DirectoryExists:
        ...

    output_fs.writetext(str(res.output_path), output_style)

    return res


class ProcessSCSS(Task):
    def run(self, state: State) -> Any:
        fs = state.multi_fs

        for path in fs.walk.files(filter=["*.html"]):
            if Path(path).name.startswith("_"):
                continue

            html_file = HtmlFile.from_source(
                path=path,
                content=fs.readtext(path)
            )

            soup = html_file.soup()
            has_modifications = False

            link_tags = soup.find_all("link")
            for tag in filter(lambda x: "href" in x.attrs, link_tags):
                self._log.info(f"Compiling SCSS file from link[href]: " + tag.attrs["href"])
                res = _compile_scss(fs, state.build_fs, tag.attrs["href"])

                if res.error:
                    log.error(res.message)
                    continue

                if res.skip:
                    log.info(res.message)
                    continue

                tag.attrs["href"] = res.output_href
                tag.attrs.pop(",", None)  # TODO: Find source of the mystery attribute

                has_modifications = True

            if has_modifications:
                html_file = html_file.with_new_content(soup.prettify())
                state.build_fs.writetext(html_file.path, html_file.content)

from typing import Any, Optional

from fs.errors import DirectoryExists
from fs.osfs import OSFS

from bread_blog.models.state import State
from bread_blog.etl.task import Task
from bread_blog.util.string_util import split_by_slash


def _make_prefix_absolute(prefix: str):
    path_parts = ["", *split_by_slash(prefix)]
    return "/".join(path_parts)


def _prepend_prefix_to_path(path: str, prefix: str):
    path_parts = [
        "",
        *split_by_slash(prefix),
        *split_by_slash(path)
    ]

    return "/".join(path_parts)


class WriteToDisk(Task):
    def run(self, state: State) -> Any:
        state.options.output_root.mkdir(parents=True, exist_ok=True)

        source_fs = state.build_fs
        output_fs = OSFS(str(state.options.output_root))

        absolute_prefix: Optional[str] = None
        if state.configuration.prefix:
            absolute_prefix = _make_prefix_absolute(state.configuration.prefix)

        def target_path(p: str) -> str:
            if absolute_prefix:
                return _prepend_prefix_to_path(p, absolute_prefix)

            return p

        for path in sorted(source_fs.walk.dirs(), key=len, reverse=True):
            try:
                self._log.info(f"Provisioning directory {path}")
                output_fs.makedirs(target_path(path))

            except DirectoryExists:
                ...

        self._log.info("Writing built project to disk")

        for path in source_fs.walk.files():
            content = source_fs.readbytes(path)
            output_fs.writebytes(target_path(path), content)

        self._log.info("Done writing project!")

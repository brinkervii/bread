from bread_blog.info.markdown_file import MarkdownFile
from bread_blog.models.state import State
from bread_blog.etl.task import Task


class CollectMarkdown(Task):
    def run(self, state: State):
        markdown_files = []

        for path in state.project_fs.walk.files(filter=["*.md"]):
            self._log.info(f"Collecting markdown file '{path}")

            markdown_files.append(
                MarkdownFile.from_source(
                    path=path,
                    source=state.project_fs.readtext(path)
                )
            )

        return markdown_files

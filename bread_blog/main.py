import logging
import sys
from pathlib import Path
from typing import Tuple

import click
import yaml

from bread_blog.models.build_options import BuildOptions
from bread_blog.models.project_configuration import ProjectConfiguration
from bread_blog.models.state import State
from bread_blog.etl.task_runner import TaskRunner
from bread_blog.tasks.collect_html import CollectHtml
from bread_blog.tasks.collect_markdown import CollectMarkdown
from bread_blog.tasks.collect_resources import CollectResources
from bread_blog.tasks.process_scss import ProcessSCSS
from bread_blog.tasks.render_markdown import RenderMarkdown
from bread_blog.tasks.render_templates import RenderTemplates
from bread_blog.tasks.make_links_absolute import MakeLinksAbsolute
from bread_blog.tasks.write_to_disk import WriteToDisk

log = logging.getLogger(__name__)


def _load_project_configuration() -> Tuple[Path, ProjectConfiguration]:
    project_configuration_path = Path("bread-blog.project.yml").resolve()
    assert project_configuration_path.exists(), "Project configuration file not found!"

    with project_configuration_path.open("r") as fp:
        data = yaml.load(fp, yaml.CSafeLoader)

    configuration = ProjectConfiguration.parse_obj(data)
    log.info(f"Loaded project configuration: {configuration.json(indent=2)}")

    return project_configuration_path, configuration


@click.group()
def cli():
    logging.basicConfig(stream=sys.stderr, level=logging.INFO)


@cli.command()
def build():
    project_configuration_path, configuration = _load_project_configuration()

    project_root = Path(".")
    if configuration_root := configuration.project_root:
        log.info(f"Found configuration project root {configuration_root}")

        configuration_root_as_path = Path(configuration_root)

        if configuration_root_as_path.is_absolute():
            log.info(f"Configuration root {configuration_root} is an absolute path")
            project_root = configuration_root_as_path

        else:
            project_root = project_configuration_path.parent / configuration_root
            project_root = project_root.resolve()

        log.info(f"Resolved project root to {project_root} using configuration root {configuration_root}")

    log.info(f"Project root is set to {project_root}")

    output_root = Path("dist").resolve()
    if configuration_root := configuration.output_root:
        log.info(f"Found configuration output root {configuration}")

        configuration_root_as_path = Path(configuration_root)

        if configuration_root_as_path.is_absolute():
            log.info(f"Configuration output root {configuration_root} is an absolute path")
            output_root = configuration_root_as_path

        else:
            output_root = project_configuration_path.parent / configuration_root
            output_root = output_root.resolve()

    log.info(f"Project output root is set to {output_root}")

    runner = TaskRunner()

    def tax_collector(task_id: str):
        ls = []

        for item in runner.collect_results(task_id):
            ls.extend(item)

        return ls

    build_options = BuildOptions(
        project_root=project_root,
        output_root=output_root
    )

    log.info(f"Project build options: {build_options.json(indent=2)}")

    state = State(
        options=build_options,
        configuration=configuration,
        markdown_provider=lambda: tax_collector("collect_markdown"),
        html_provider=lambda: tax_collector("collect_html")
    )

    runner.add_tasks([
        CollectMarkdown(task_id="collect_markdown"),
        RenderMarkdown(task_id="render_markdown", depends_on=["collect_markdown"]),
        CollectHtml(task_id="collect_html", depends_on=["collect_markdown", "render_markdown"]),
        RenderTemplates(task_id="render_templates", depends_on=["collect_html"]),
        ProcessSCSS(task_id="process_scss", depends_on=["render_templates", "collect_html"]),
        CollectResources(task_id="collect_resources", depends_on=["render_templates"])
    ])

    if configuration.prefix:
        runner.add_tasks([
            MakeLinksAbsolute(
                task_id="make_links_absolute",
                depends_on=["collect_resources", "render_templates"]
            )
        ])

    runner.add_tasks([WriteToDisk(task_id="write_to_disk")])

    runner.run(state)


if __name__ == "__main__":
    cli()
